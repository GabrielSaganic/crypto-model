import pandas as pd
import sys

DataFrame = pd.read_csv(sys.argv[1])

DataFrame['Date'] = pd.to_datetime(DataFrame['Date']).dt.date

DataFrame = DataFrame.sort_values(by=['Date'], ascending = False)

print(DataFrame.tail(10))

DataFrame.to_csv(sys.argv[1], mode='w',index=False)
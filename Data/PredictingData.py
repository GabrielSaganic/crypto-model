# import tensorflow as tf
# from tensorflow.keras.models import Sequential
# from tensorflow.keras.layers import LSTM, Dense, Dropout, Bidirectional
# from tensorflow.keras.callbacks import ModelCheckpoint, TensorBoard
# from sklearn.model_selection import train_test_split
# from collections import deque
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import mean_squared_error
import re

import matplotlib.pyplot as plt
import os
import math
import numpy as np
import pandas as pd
import datetime 
import random

from statsmodels.tsa.vector_ar.var_model import VAR

def AddDays(learningData,days):
    for i in range(days):
        endDay = (datetime.datetime.strptime(str(learningData.iloc[-1]['Date']), "%Y-%m-%d") + datetime.timedelta(days=1)).date()
        newRow = {'Date': endDay}

        learningData = learningData.append(newRow, ignore_index=True)

    return(learningData)

learningData = pd.read_csv(('Result/ResultData.csv'))
cols = learningData.columns
#predictedData = AddDays(learningData,5)
# np.random.seed(314)
# tf.random.set_seed(314)
# random.seed(314)


 # split the dataset into training & testing sets by date (not randomly splitting)
trainData = learningData[:int(0.95*(len(learningData)))]
testData  = learningData[int(0.95*(len(learningData))):]

trainData = trainData.drop(columns = ['Date'])
dateList = testData['Date'].tolist()

testData = testData.drop(columns = ['Date'])

colsWithoutDate = testData.columns


trainData = np.asarray(trainData)
testData = np.asarray(testData)


model = VAR(endog=trainData)
model_fit = model.fit()
prediction = model_fit.forecast(model_fit.y, steps=len(testData))

pred = pd.DataFrame(index=range(0,len(prediction)),columns=[cols + 'Pred'])

for j in range(0,len(cols)):
    for i in range(0, len(prediction)):
        if(j==0):
            pred.iloc[i][j] = dateList[i]
        else:
            pred.iloc[i][j] = prediction[i][j-1]

pred.to_csv('PredictingFolder/ConvertNotImportant.csv', mode='w',index=False)

pred = pd.read_csv(('PredictingFolder/ConvertNotImportant.csv'))

learningData['Date'] = pd.to_datetime(learningData['Date']).dt.date
pred['DatePred'] = pd.to_datetime(pred['DatePred']).dt.date

learningData = learningData.merge(pred,how='left', left_on='Date', right_on='DatePred')
learningData = learningData.drop(['DatePred'], axis=1)

endDate = learningData.iloc[-(len(testData)+1)]['Date']

#print(learningData)
for i in range((len(testData)),0,-1):
    if(i == (len(testData))):
        if(learningData.iloc[-(i + 1)][2] <  learningData.iloc[-i][5]):
            print(str(learningData.iloc[-(i)]['Date']) + ' Day before: ' + str(learningData.iloc[-(i + 1)][2]) + ' Predicted: ' + str(learningData.iloc[-i][5]) + ' Indicator: ▲') 
        elif(learningData.iloc[-(i + 1)][2] >  learningData.iloc[-i][5]):
            print(str(learningData.iloc[-(i)]['Date']) + ' Day before: ' + str(learningData.iloc[-(i + 1)][2]) + ' Predicted: ' + str(learningData.iloc[-i][5]) + ' Indicator: ▼') 
        elif(learningData.iloc[-(i + 1)][2] ==  learningData.iloc[-i][5]):
            print(str(learningData.iloc[-i]['Date']) + ' Day before: ' + str(learningData.iloc[-(i + 1)][2]) + ' Predicted: ' + str(learningData.iloc[-i][5]) + ' Indicator: =') 
    else:
        if(learningData.iloc[-(i+1)][5] <  learningData.iloc[-i][5]):
            print(str(learningData.iloc[-(i)]['Date']) + ' Day before: ' + str(learningData.iloc[-(i + 1)][5]) + ' Predicted: ' + str(learningData.iloc[-i][5]) + ' Indicator: ▲') 
        elif(learningData.iloc[-(i+1)][5] >  learningData.iloc[-i][5]):
            print(str(learningData.iloc[-(i)]['Date']) + ' Day before: ' + str(learningData.iloc[-(i + 1)][5]) + ' Predicted: ' + str(learningData.iloc[-i][5]) + ' Indicator: ▼') 
        elif(learningData.iloc[-(i+1)][5] ==  learningData.iloc[-i][5]):
            print(str(learningData.iloc[-i]['Date']) + ' Day before: ' + str(learningData.iloc[-(i + 1)][5]) + ' Predicted: ' + str(learningData.iloc[-i][5]) + ' Indicator: =') 


print('Only indicator:')
learningData['IndicaotorPred'] = learningData['IndicaotorPred'].round(0)
for i in range((len(testData)),0,-1):
    if(learningData.iloc[-(i)][6] == 1):
        print(str(learningData.iloc[-(i)]['Date']) + ' Indicator: ▲') 
    elif(learningData.iloc[-(i)][6] == 0):
        print(str(learningData.iloc[-(i)]['Date'])  + ' Indicator: ▼') 
    
        


colsWithoutDate = learningData.columns.tolist()
colsWithoutDate.remove('Date')
# colsWithoutDate.remove(regex = 'RowCount')

regex = re.compile(r'.*RowCount.*')

colsWithoutDate = [i for i in colsWithoutDate if not regex.match(i)]

print(colsWithoutDate)
#colsWithoutDate.remove('DatePred')

learningData.to_csv('PredictingFolder/PedictedValues.csv', mode='w',index=False)


learningData.plot(x='Date' , y=colsWithoutDate)
plt.savefig('PredictingFolder/PedictedValuePlot.png')
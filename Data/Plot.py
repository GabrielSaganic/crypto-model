import pandas as pd
import matplotlib.pyplot as plt
import sys


columnsName:list =[]
DataList:list = []
DataListRow:list = []

sys.argv.pop(0)
mode = sys.argv[0]
sys.argv.pop(0)
cryptoCurrency = list(sys.argv[0])
sys.argv.pop(0)

if 'B' in cryptoCurrency:
    BitcoinPrice = pd.read_csv('Price/BitcoinPrice.csv', usecols=['Date','Closing Price (USD)'])
    BitcoinPrice.columns = ['Date','BitcoinPrice']

if 'E' in cryptoCurrency:
    EthereumPrice = pd.read_csv('Price/EthereumPrice.csv', usecols=['Date','Close'])
    EthereumPrice.columns = ['Date','EthereumPrice']

#print(EthereumPrice)
#print(BitcoinPrice)
if(mode == '-w' or mode =='-a' or mode =='-r'):
    #Citanje csv
    for _data in sys.argv:
        DataList.append(pd.read_csv(_data, usecols=['Date','PostMessage']))
        columnsName.append(_data.replace(".csv",""))

    DataListRow = DataList.copy()
    columnsNameRow = columnsName.copy()

    #po rijecima
    if(mode == '-w' or mode =='-a'):
        #Zbrajanje spoiminjanja rijeci
        for i in range(len(DataList)):
        #for _data in DataList:  
            if 'B' == cryptoCurrency[i]:
                DataList[i]['PostMessage'] = DataList[i].PostMessage.str.count("BTC|bitcoin")

            if 'E' in cryptoCurrency[i]:
                DataList[i]['PostMessage'] = DataList[i].PostMessage.str.count("ETH|ethereum|etherum")
        
        # for _data in DataList:
        #     _data['Date'] = pd.to_datetime(_data['Date']).dt.date
        
        # for _data in DataList:
        #     print(_data)
        #Preimenovanje stupaca
        i=0
        for _data in DataList:  
            _data.columns = ['Date',str(columnsName[i]) + 'WordCount' ]
            i = i+1 

        for i in range(len(columnsName)):
            columnsName[i] = str(columnsName[i]) + 'WordCount'
        #Sumiranje spominjanje rijeci po datumu
        i=0
        for _data in DataList:    
            DataList[i] = _data.groupby(['Date'],as_index=False).sum()
            i=i+1

    #------------------------- po retcima
    if(mode == '-r' or mode == '-a'):
        i=0
        for _data in DataListRow:    
            DataListRow[i] = _data.groupby(['Date'],as_index=False).count()
            i=i+1
        
        i=0
        for _data in DataListRow:  
            _data.columns = ['Date',str(columnsNameRow[i])+'RowCount']
            i = i+1 

        for i in range(len(columnsNameRow)):
            columnsNameRow[i] = str(columnsNameRow[i]) + 'RowCount'
    
    if(mode == '-w'):
        AllData = DataList[0][['Date']].copy()
    elif(mode == '-r'):
        AllData = DataListRow[0][['Date']].copy()
    else:
        AllData = DataList[0][['Date']].copy()

    if(mode == '-w' or mode =='-a'):
        for _data in DataList:
            AllData = AllData.join(_data.set_index('Date'), on='Date')
        startDate = DataList[0].iloc[0]['Date']
        endDate = DataList[0].iloc[-1]['Date']
    
    if(mode == '-r' or mode =='-a'):
        for _data in DataListRow:  
            AllData = AllData.join(_data.set_index('Date'), on='Date')
        startDate = DataListRow[0].iloc[0]['Date']
        endDate = DataListRow[0].iloc[-1]['Date']
   
    AllData = AllData.fillna(0)

    if(mode == '-a'):
        columnsName = columnsName + columnsNameRow
    elif(mode == '-r'):
        columnsName = columnsNameRow.copy()

    if 'B' in cryptoCurrency:
        columnsName.append('BitcoinPrice')
        BitcoinPrice = BitcoinPrice[BitcoinPrice.Date.between(startDate, endDate)]
        AllData = AllData.join(BitcoinPrice.set_index('Date'), on='Date')

    if 'E' in cryptoCurrency:
        columnsName.append('EthereumPrice')
        EthereumPrice = EthereumPrice[EthereumPrice.Date.between(startDate, endDate)]
        AllData = AllData.join(EthereumPrice.set_index('Date'), on='Date')

    for column in columnsName:
        AllData[str(column)] = (AllData[str(column)] - AllData[str(column)].min()) / (AllData[str(column)].max() - AllData[str(column)].min())     

    AllData.to_csv('Result/ResultData.csv', mode='w',index=False)
    AllData.plot(x='Date' , y=columnsName)
    plt.savefig('Result/ResultPlot.png')

    print("Plot spremljen u Result/ResultPlot.png, CSV spremljen u Result/ResultData.csv")
else:
    print("Unesi argument -r za ispis grafa po broju retka, -w za ispis grafa po broju rijeci, -a za ispis oba")


#------------------------------------------------------------------
# #Citanje iz csv
# BitcoinPrice = pd.read_csv('BitcoinPrice.csv', usecols=['Date','Closing Price (USD)'])
# BitcoinData = pd.read_csv('TestBitcoinData.csv', usecols=['Date','PostMessage'])


# #Grupiranje po datumu
# BitcoinData = BitcoinData.groupby(['Date'],as_index=False).count()

# #Preimenovanje headersa
# BitcoinData.columns = ['Date','PostCount']
# BitcoinPrice.columns = ['Date','BitcoinPrice']

# #Neka stupci budu datatime -- mozda ne treba
# # BitcoinData['Date'] = pd.to_datetime(BitcoinData['Date'])
# # BitcoinPrice['Date'] = pd.to_datetime(BitcoinPrice['Date'])

# #Uzimanje redaka u određenom datumu
# startDate = BitcoinData.iloc[0]['Date']
# endDate = BitcoinData.iloc[-1]['Date']
# BitcoinPrice = BitcoinPrice[BitcoinPrice.Date.between(startDate, endDate)]

# #Joinanje datframe po datumu
# AllData = BitcoinData.join(BitcoinPrice.set_index('Date'), on='Date')

# #print(BitcoinData)

#Normalizacija
# AllData['PostCount'] = (AllData['PostCount'] - AllData['PostCount'].min()) / (AllData['PostCount'].max() - AllData['PostCount'].min())     
# AllData['BitcoinPrice'] = (AllData['BitcoinPrice'] - AllData['BitcoinPrice'].min()) / (AllData['BitcoinPrice'].max() - AllData['BitcoinPrice'].min())     
  

# print(AllData)

# #Izrada plota i spremnanje u png
# AllData.plot(x='Date' , y=['PostCount','BitcoinPrice'])
# plt.savefig('Test.png')
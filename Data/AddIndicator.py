import pandas as pd
import sys

DataFrame = pd.read_csv(sys.argv[1])

DataFrame['Indicaotor'] = ''

_tmpVar = 0
_indicaotor = 0
_first = True
for index, row in DataFrame.iterrows():
    if(_first):
        _indicaotor=1
        _tmpVar = row['BitcoinPrice']
        _first = False

    elif (_tmpVar <= row['BitcoinPrice']):
        _indicaotor=1
        _tmpVar = row['BitcoinPrice']

    elif (_tmpVar > row['BitcoinPrice']):
        _indicaotor=0
        _tmpVar = row['BitcoinPrice']

    DataFrame.at[index,'Indicaotor'] = _indicaotor
        
print(DataFrame)
DataFrame.to_csv(sys.argv[1], mode='w',index=False)
from textblob import TextBlob
import pandas as pd

DataFrame = pd.read_csv('TestData.csv')
DataFrame['Polarity'] = ''
DataFrame['Subjectivity'] = ''
print('START')
for index, row in DataFrame.iterrows():
    blob = TextBlob(row['PostMessage'])
    row['Polarity'] = blob.polarity
    row['Subjectivity'] = blob.subjectivity

DataFrame.to_csv('TestDataSentiment.csv', mode='w',index=False)

#print(DataFrame)

import pandas as pd
from statsmodels.formula.api import ols
import statsmodels.api as sm
import matplotlib.pyplot as plt
import datetime 

def LinearRegression(learningData):
    #JednostavnaLinearnaRegresija
    SimpleLinearRegressionModel = ols("EthereumPrice ~ TestEtheriumDataWordCount", data=learningData).fit()
    SimpleLinearRegressionModelSummary = SimpleLinearRegressionModel.summary()
    print(SimpleLinearRegressionModelSummary)


    fig = plt.figure(figsize=(15,8))
    fig = sm.graphics.plot_regress_exog(SimpleLinearRegressionModel, "TestEtheriumDataWordCount", fig=fig)
    plt.savefig('SimpleLinearRegresion.png')

    #Multi linearna regresija
    MultiLinearRegressionModel = ols("""EthereumPrice ~ TestEtheriumDataWordCount 
                                                + TestEtheriumDataRowCount""", data=learningData).fit()
    MultiLinearRegressionModelSummary = MultiLinearRegressionModel.summary()
    print(MultiLinearRegressionModelSummary)

    fig = plt.figure(figsize=(20,12))
    fig = sm.graphics.plot_partregress_grid(MultiLinearRegressionModel, fig=fig)
    plt.savefig('MultiLinearRegresion.png')

def Predicting(learningData):
    endDay = (datetime.datetime.strptime(str(learningData.iloc[-1]['Date']), "%Y-%m-%d") + datetime.timedelta(days=1)).date()
    newRow = {'Date': endDay}

    learningData = learningData.append(newRow, ignore_index=True)
    print(learningData)


learningData = pd.read_csv(('Result/ResultData.csv'))
#print(learningData)

Predicting(learningData)
#LinearRegression(learningData)






# crypto model
https://www.analyticsvidhya.com/blog/2018/09/multivariate-time-series-guide-forecasting-modeling-python-codes/



Za filtriranje podataka:    grep -E 'bitcoin|BTC' TestData.csv  > TestBitcoinData.csv; sed -i '1 i\Date,PostMessage,Link' TestBitcoinData.csv 

Micanje duplikata:  sort 4chanAllThreadLink.csv | uniq -u > 4chanTestLink.csv
                    awk '!visited[$0]++' 4chanAllThreadLink.csv > 4chanTestLinkAWK.csv

Prvih n za testiranje:  head -100000 4Chan/4chanAllPosts.csv > Data/TestData.csv

Upute za koristenje: 
1.) grep -E 'bitcoin|BTC' 4Chan/AllPosts.csv  > Data/BitcoinData.csv; sed -i '1 i\Date,PostMessage,Link' Data/BitcoinData.csv 
    Za uzimanje samo postova koje spominjaju 'bitcoin|BTC'
2.) cd Date
3.) python OrderByDate BitcoinData.csv
    Za pripremanje tablice
4.) python -r B BitcoinData.csv
    za grupiranje po datumu te zbrajanje koliko postova ima po datumu
5.) python PredictingData.py
    Za predikciju
    Ocito se treba dodati da ispravi di nije upisana vrijednost u Result/ResultData.csv
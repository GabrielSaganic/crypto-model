import pandas as pd
import os
import requests
from bs4 import BeautifulSoup
import multiprocessing
from multiprocessing import Process , Queue
from datetime import datetime

q: "Queue[str]" = Queue()

def getAllLink(URL,i,headers):
    try:
        tryAgain = 0
        while tryAgain < 20:
            threads = []
            allLink = []
            #print("Getting all threads from page: " + str(i))

            req = requests.get(URL,headers = headers)
            soup = BeautifulSoup (req.content, features="lxml")

            article = soup.find_all("span", class_="post_controls")

            for _a in article:
                threads.append(_a.find("a", class_="btnr parent"))

            for thread in threads:
                if thread.text == 'View':
                    link = thread.get('href')
                    allLink.append(link)
            if not allLink:
                tryAgain = tryAgain + 1
                print("Trying again getting links No. " + str(tryAgain))
            else:
                tryAgain = 100
        q.put(allLink)
    except Exception as e:
        logFile =  open('4chanLogFile.txt', 'a')
        logFile.write(str(datetime.now().strftime("%H:%M:%S")) + ' - ERROR:' + str(e) + '\n' + '\tLink: ' + str(URL) + '\n')
        logFile.close()
        
def getMessages(thread,headers):
    try:
        tryAgain = 0
        while tryAgain < 20:
            #print("Gettin posts from thread: " + str(thread))
            posts = pd.DataFrame(columns=['Date','PostMessage','Link'])

            postMessage = []
            postTime = []

            req = requests.get(thread,headers = headers)
            soup = BeautifulSoup (req.content, features="lxml")

            text = soup.find_all("div", class_="text")
            time = soup.find_all("span", class_="time_wrap")

            for _time in time:
                postTime.append(_time.find("time", recursive=False).text)
            for _t in text:
                postMessage.append(_t.text)

            for i in range(len(postMessage)):
                posts = posts.append({'Date': postTime[i], 'PostMessage':postMessage[i],'Link': thread}, ignore_index=True)

            if posts.empty:
                tryAgain = tryAgain + 1
                print("Trying again get posts from thread No. " + str(tryAgain))
            else:
                posts.to_csv('4chanAllPosts.csv', header=None, mode='a',index=False)
                tryAgain = 100
    except Exception as e:
        print("greska")
        logFile =  open('4chanLogFile.txt', 'a')
        logFile.write(str(datetime.now().strftime("%H:%M:%S")) + ' - ERROR:' + str(e) + '\n' + '\tLink: ' + str(URL) + '\n')
        logFile.close()  

def main(new):
    logFile =  open('4Chan/4chanLogFile.txt', 'a')
    logFile.write(str(datetime.now().strftime("%H:%M:%S")) + ': Started' + '\n')
    logFile.close()  
    headers = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:82.0) Gecko/20100101 Firefox/82.0'}
    URL = "https://archived.moe/biz/page"

    allThread = []
    pageThread = []
    processList = []
    CPU = multiprocessing.cpu_count()
    lastPage = False

    if(new != "2"):
        page = pd.read_csv('4Chan/4chanAllThreadLink.csv').Page.tolist()
        if not page:
            i = 1
        else:
            i=page[-1] + 1

        logFile =  open('4Chan/4chanLogFile.txt', 'a')
        logFile.write(str(datetime.now().strftime("%H:%M:%S")) + ' - Started getting links from page ' + str(i) +'\n')
        logFile.close()

        while lastPage == False:
            for j in range(CPU):
                processList.append(Process(target=getAllLink, args=(URL + "/" + str(i + j), i+j,headers)))

            for p in processList:
                p.start()

            for p in processList:
                p.join()

            if(q.empty() == False):
                for k in range(q.qsize()):
                    pageThread = pageThread + q.get()

            if not pageThread:
                lastPage = True
            else:
                df_all_thread = pd.DataFrame({'Link':pageThread , 'Page': i + CPU - 1})
                df_all_thread.to_csv('4Chan/4chanAllThreadLink.csv',header=None, mode='a', index=False)
            i = i + CPU
            # if(i % 100) == 0:
            #     print("Complete 100 page. Now on page: " + str(i))
            pageThread.clear()
            processList.clear()
    allThread = pd.read_csv('4Chan/4chanAllThreadLink.csv').Link.tolist()

    
    if(new != "1"):
        
        lastLink = pd.read_csv("4Chan/4chanAllPosts.csv").Link.tolist()[-1]
        if lastLink != "":
            index = allThread.index(lastLink) + 1
            print("Nastavlja od linka " + str(lastLink) + " Rb. " + str(index))
        else: 
            index = 0
            print("Pocinje scrapati postove iz pocetka")

        logFile =  open('4Chan/4chanLogFile.txt', 'a')
        logFile.write(str(datetime.now().strftime("%H:%M:%S")) + ' - Started getting thread from link ' + str(allThread[index]) + ' No. ' + str(index)+ '\n')
        logFile.close()
        
        printCounter = 0

        for i in range(index, len(allThread), CPU):
            printCounter = printCounter + CPU
            if(printCounter > 100):
                print(str(i+CPU) + "/" + str(len(allThread)) + "\n")
            
            for j in range(CPU):
                processList.append(Process(target=getMessages, args=(allThread[i + j], headers)))
            for p in processList:
                p.start()
            for p in processList:
                p.join()
            processList.clear()

if __name__ == "__main__":
    addHeaders:list = [] 
    new = input("Unesi 0 za pokretanje iz pocetka i brisanje csv: ")

    if(new == "0"):
        header = pd.DataFrame({'Date':addHeaders,'PostMessage':addHeaders,'Link':addHeaders})
        header.to_csv('4Chan/4chanAllPosts.csv', mode='w', index=False)

        # header = pd.DataFrame({'Link':addHeaders , 'Page': addHeaders})
        # header.to_csv('4Chan/4chanAllThreadLink.csv', mode='w', index=False)

        logFile =  open('4Chan/4chanLogFile.txt', 'w')
        logFile.write('')
        logFile.close()
    new = input("Unesi 1 za skidanje samo url, 2 za svih tredova: ")

    main(new)
